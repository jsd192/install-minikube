#!/bin/bash
#
dnf install curl vim -y

##instalar minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

minikube version

##instalar kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

chmod +x kubectl

mv kubectl /usr/local/bin

kubectl version --client -o json

yum install -y yum-utils
##instalar docker
yum-config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

systemctl enable --now docker

docker version

usermod -aG docker $USER

minikube start --driver=docker --force
minikube status

##creacion del servicio minikube para que inicie con el sistema
cd /etc/systemd/system/
cat > minikube.service << EOF
[Unit]
Description=Minikube
Documentation=https://minikube.sigs.k8s.io/docs/

[Service]
ExecStart=/usr/local/bin/minikube start --driver=docker --force
#ExecStop=/usr/local/bin/minikube stop
#Restart=always
#RestartSec=10
User=root
Group=root

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable minikube.service
systemctl start minikube.service

cd

#crear un namespace
kubectl create namespace yape-personas

#creacion yml para las replicas
cat > replicas-minikube.yml << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: public-interop-transfer-result-v2 #nombre de los pods a crear
  namespace: yape-personas #nombre del namespace a usar
spec:
  replicas: 10  # Número de réplicas que deseas (en este caso, 10)
  selector:
    matchLabels:
      app: public-interop-transfer-result-v2
  template:
    metadata:
      labels:
        app: public-interop-transfer-result-v2
    spec:
      containers:
      - name: nginx-contenedor #le ponemos un nombre del contenedor
        image: nginx:latest #imagen a usar
EOF

#ejecutar el yml para las replicas
kubectl apply -f replicas-minikube.yml

clear
echo "Luego de 1 minuto mostrará los pods creados en namespace yape-personas"
sleep 1m
kubectl get pods -n yape-personas -l app=public-interop-transfer-result-v2


